#!/bin/bash

function pull_code(){
    local result

    if [ -d "$CLONEDIR" ]
    then
        echo "Pulling latest IFAS installer..."
        cd $CLONEDIR
        git pull origin master
        result=$?
        cd -
    else
        echo "Fetching full IFAS installer..."
        git clone https://$BBUSER@bitbucket.org/ifas/ifas_installer.git $CLONEDIR
        result=$?
    fi

    if [ $result -ne 0 ]
    then
        printf "Authentication failed. Please try again.\n"
        return 1
    else
        return 0
    fi
}

BBUSER=$1
HOSTNAME=`hostname`
CLONEDIR="ifas_installer"

if [ "$1" = "-h" ] || [ -z "$BBUSER" ]; then
    echo "Usage: ./install.sh <Bitbucket username>"
    exit
fi

# cache passwords for 30 mins to avoid retyping
git config --global credential.helper 'cache --timeout=1800'

read -p "I have your hostname as $HOSTNAME: correct (y/n)? "

[ "$REPLY" == "y" ] || exit

sudo apt-get -y install aptitude
sudo aptitude -y install python-jinja2 git curl python-pip
sudo pip install fabric==1.8.1

pull_result=1

while [ $pull_result != 0 ]
do
    pull_code
    pull_result=$?
done

cd $CLONEDIR && fab --set gituser=$BBUSER --user=$USER --hosts=$HOSTNAME full_setup
